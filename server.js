var util = require('util')
var fs = require('fs')

var app = require('express').createServer();

app.get('/', function(req, res) {
	res.write('Hello from node test\n');

	res.write('print process.version\n');
	res.write(util.inspect(process.version, true, null) + '\n');
	res.write('print process.versions\n');
	res.write(util.inspect(process.versions, true, null) + '\n');
	res.end('\n');
});
app.listen(process.env.VCAP_APP_PORT || 3000);
